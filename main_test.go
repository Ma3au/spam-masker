package main

import "testing"

func TestMaskerWebAddress(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{"http://google.com", "http://**********"},
		{"http://google.com/ru", "http://*************"},
		{"Here's my spammy page: http://hehefouls.netHAHAHA see you.", "Here's my spammy page: http://******************* see you."},
		{"Not a web address", "Not a web address"},
	}

	for _, test := range tests {
		result := MaskerWebAddress(test.input)
		if result != test.expected {
			t.Errorf("Expected %s but got %s for input '%s'", test.expected, result, test.input)
		}
	}
}
