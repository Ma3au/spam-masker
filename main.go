package main

import (
	"bufio"
	"fmt"
	"os"
	"bytes"
)

// Функция маскировки веб-адреса
func MaskerWebAddress(s string) string {
    
    //Создаем байтовый срез, куда помещаем переданную строку
    buffer := []byte(s)
    
    /* Проходимся по срезу пока не встретим "http://", вычисляем индекс
    конца ссылки и маскируем её "звёздочками" */
    for i := 0; i < len(buffer); i++ {
		if (i+6) < len(buffer) && string(buffer[i:(i+7)]) == "http://" {
			index := bytes.Index(buffer[(i+7):], []byte(" "))
			if index < 0 {
			    index = len(buffer) - (i+7)
			}
			slice := bytes.Repeat([]byte("*"), index)
			buffer = bytes.Replace(buffer, buffer[(i+7):(i+7+index)], slice, 1)
		}
	}
    
    // Выводим результат в виде строки
    return string(buffer)
}

func main() {
	//Производим чтение из Stdin
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()

	//Выводим результат функции в терминал
	fmt.Println(MaskerWebAddress(text))
}